package com.example.nycschools.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.nycschools.model.SATScores

@Dao
interface SATScoresDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(scores: List<SATScores>)

    @Query("DELETE FROM sat_table")
    fun deleteAll()

    @Query("SELECT * FROM sat_table where dbn = :schoolDBN")
    fun getScore(schoolDBN: String?): LiveData<SATScores>

    @Query("SELECT * FROM sat_table  ORDER BY school_name ASC")
    fun allScores(): LiveData<List<SATScores>>
}