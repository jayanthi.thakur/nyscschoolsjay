package com.example.nycschools.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.nycschools.model.School

@Dao
interface SchoolDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(school: List<School>)

    @Query("DELETE FROM school_table")
    fun deleteAll()

    @Query("SELECT * FROM school_table ORDER BY school_name ASC")
    fun schools(): LiveData<List<School>>

    @Query("SELECT * FROM school_table where school_name like :searchString ORDER BY school_name ASC")
    fun getSchoolsFiltered(searchString: String?): LiveData<List<School>>
}