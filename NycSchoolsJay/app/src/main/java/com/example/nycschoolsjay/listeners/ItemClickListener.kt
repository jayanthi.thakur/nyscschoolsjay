package com.example.nycschoolsjay.listeners

import com.example.nycschools.model.School

interface ItemClickListener {
    fun onSchoolSelected(school: School)
}