package com.example.nycschools.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.nycschools.dao.SchoolRoomDatabase
import com.example.nycschools.model.SATScores
import com.example.nycschools.model.School
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.*
import java.io.IOException

/**
 * In repository wanted to save the data in roon data base and then fetch and display but for some reason am unable to fetch the
 * data from room database so i left the code as it is and stored the data temporarily in a static variable to display for nor
 * and also wanted to use retrofit to make calls as it gives good callback functions.
 * **/
class NycSchoolRepository(context: Context) {

    //var mSchoolDao: SchoolDao
   //var mScoresDao: SATScoresDao
    private val NYC_SCHOOLS_URL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    private val NYC_SCHOOLS_SAT_SCORES_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    private val client = OkHttpClient()


    init {
        initialiseDB(context)
        //mSchoolDao = schoolRoomDatabase!!.schoolDao()
        //mScoresDao = schoolRoomDatabase!!.satScoresDao()
        //mAllSchools = mSchoolDao.schools()
    }

    companion object {
        var mAllSchools: List<School> = ArrayList()
        var mSatScores: List<SATScores> = ArrayList()
        var schoolRoomDatabase: SchoolRoomDatabase? = null
        private fun initialiseDB(context: Context) {
            schoolRoomDatabase =  SchoolRoomDatabase.getDatabase(context)
        }

    }

    /**
     * Will fetch Schools list as LiveData so that it can be executed in the background
     * @return
     */
    fun getAllSchools(): List<School> {
        //initialiseDB(context)
        return  mAllSchools
    }

//    /**
//     * Insert Schools into DB in background
//     * @param schools
//     */
//    fun insertAll(schools: List<School>) {
//        SchoolRoomDatabase.databaseWriteExecutor.execute { mSchoolDao.insertAll(schools) }
//    }

    /**
     * From here lies all code related to REST API calls using OKHTTP.
     * We can put them in another class to handle them.
     */
    fun loadSchools() {
        fetchSchoolsData()
        fetchSATScores()
    }

    /**
     * Get SATScores for School DBN
     * @param schoolDBN
     * @return
     */
    fun getSATScoresForSchool(): List<SATScores> {
        //return mScoresDao.getScore(schoolDBN)
        return mSatScores;
    }


    /**
     * Fetches School Data from NYC Schools API
     */
    fun fetchSchoolsData() {
        val request = Request.Builder()
            .url(NYC_SCHOOLS_URL)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) throw IOException("Unexpected code $response")

                    val jsonData = response.body!!.string()
                    // Load data as School Object using Gson
                    val listType = object : TypeToken<List<School?>?>() {}.type
                    val schools = Gson().fromJson<List<School>>(jsonData, listType)
                    mAllSchools = schools
                }
            }
        })

    }

    fun fetchSATScores(){
        val request = Request.Builder()
            .url(NYC_SCHOOLS_SAT_SCORES_URL)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) throw IOException("Unexpected code $response")

                    val jsonData = response.body!!.string()
                    // Load data as School Object using Gson
                    val listType = object : TypeToken<List<SATScores?>?>() {}.type
                    val satScores = Gson().fromJson<List<SATScores>>(jsonData, listType)
                    mSatScores = satScores
                }
            }
        })
    }

}