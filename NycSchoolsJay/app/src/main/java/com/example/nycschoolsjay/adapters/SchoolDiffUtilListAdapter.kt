package com.example.nycschoolsjay.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.model.School
import com.example.nycschoolsjay.R
import com.example.nycschoolsjay.listeners.ItemClickListener

class SchoolDiffUtilListAdapter :
    ListAdapter<School, SchoolDiffUtilListAdapter.SchoolSimpleViewHolder>(DiffUtils()) {
    private lateinit var itemClick : ItemClickListener
    class SchoolSimpleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.findViewById(R.id.textView)

        fun binds(item: School) {
            title.text = item.school_name
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolSimpleViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.schools_simple_recyclerview_item, parent, false)
        return SchoolSimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder: SchoolSimpleViewHolder, position: Int) {
        val item = getItem(position)
        holder.binds(item)
        holder.itemView.setOnClickListener {
            itemClick.onSchoolSelected(item)
        }
    }

    fun setItemClickListener(itemClick : ItemClickListener) {
        this.itemClick = itemClick
    }

    class DiffUtils : DiffUtil.ItemCallback<School>() {
        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.school_name == newItem.school_name
        }
    }

   }