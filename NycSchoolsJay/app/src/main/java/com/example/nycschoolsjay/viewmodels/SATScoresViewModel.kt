package com.example.nycschoolsjay.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.nycschools.model.SATScores
import com.example.nycschools.repository.NycSchoolRepository

class SATScoresViewModel(application: Application) : AndroidViewModel(application) {
    private val mRepository: NycSchoolRepository

    init {
        mRepository = NycSchoolRepository(application.applicationContext)
    }

    fun getScoresForSchool(schoolDBN: String?): SATScores {
        val map = mRepository.getSATScoresForSchool().associateBy { it.dbn }
        val sDbnValue: String = if (schoolDBN != null) schoolDBN else "01M292"
        val satScores : SATScores? = map[sDbnValue]
        return satScores!!
    }
}