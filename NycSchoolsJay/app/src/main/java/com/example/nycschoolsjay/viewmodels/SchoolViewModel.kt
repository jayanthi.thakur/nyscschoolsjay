package com.example.nycschools.viewmodels

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.nycschools.model.School
import com.example.nycschools.repository.NycSchoolRepository

class SchoolViewModel(application: Application) : AndroidViewModel(application) {
    var mRepository: NycSchoolRepository
    //var allSchools: LiveData<List<School>>
    val allSchools = MutableLiveData<List<School>>()



    init {
        mRepository = NycSchoolRepository(application.applicationContext)
        //allSchools = mRepository.getAllSchools(application.applicationContext)
    }

    fun loadSchools() {
        mRepository.loadSchools()
    }

    fun getSchools() {
        allSchools.value = mRepository.getAllSchools()
    }
}