package com.example.nycschoolsjay.activities

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.nycschools.model.SATScores
import com.example.nycschoolsjay.R
import com.example.nycschoolsjay.viewmodels.SATScoresViewModel

class SchoolActivity : AppCompatActivity() {

    private lateinit var satScoresViewModel: SATScoresViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_school)
        //val toolbar = findViewById<Toolbar>(R.id.school_toolbar)
        //setSupportActionBar(toolbar)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        //School object to setup the page
        val schoolDBN = intent.getStringExtra ("SchoolDbn")
        Log.i("schoolDBN", "" + schoolDBN)
        satScoresViewModel = ViewModelProvider(this)[SATScoresViewModel::class.java]
        val satScores : SATScores = satScoresViewModel.getScoresForSchool(schoolDBN)
        // Setup UI with School object from intent
        val schoolNameTextView = findViewById<TextView>(R.id.schoolNameText)
        schoolNameTextView.setText(satScores.school_name)
        schoolNameTextView.isSingleLine = false
        schoolNameTextView.setHorizontallyScrolling(false)

        // Loads SAT Scores for the School
        satScoresUpdated(satScores)
    }

    /**
     * Updates the SAT Scores once available from DB
     * @param score
     */
    private fun satScoresUpdated(score: SATScores?) {
        if (score != null) {
            val satScoresTextView = findViewById<TextView>(R.id.satScoresTextView)
            satScoresTextView.setText(score.toString())
        }
    }

}