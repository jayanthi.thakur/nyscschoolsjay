package com.example.nycschoolsjay.adapters

import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import android.content.Intent
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.example.nycschools.model.School
import com.example.nycschoolsjay.R
import com.example.nycschoolsjay.activities.SchoolActivity

class SchoolSimpleViewHolder private constructor(itemView: View) :
    RecyclerView.ViewHolder(itemView) {
    private val schoolItemView: TextView
    private var school: School? = null

    init {
        itemView.setOnClickListener { v ->
            val intent = Intent(v.context, SchoolActivity::class.java)
            intent.putExtra("School", school)
            v.context.startActivity(intent)
        }
        schoolItemView = itemView.findViewById(R.id.textView)
    }

    fun bind(school: School) {
        this.school = school
        schoolItemView.text = school.school_name
    }

    companion object {
        @JvmStatic
        fun create(parent: ViewGroup): SchoolSimpleViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.schools_simple_recyclerview_item, parent, false)
            return SchoolSimpleViewHolder(view)
        }
    }
}