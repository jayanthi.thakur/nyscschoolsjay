package com.example.nycschools.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nycschools.model.School
import com.example.nycschools.viewmodels.SchoolViewModel
import com.example.nycschoolsjay.R
import com.example.nycschoolsjay.activities.SchoolActivity
import com.example.nycschoolsjay.adapters.SchoolDiffUtilListAdapter
import com.example.nycschoolsjay.databinding.ActivityMainBinding
import com.example.nycschoolsjay.listeners.ItemClickListener

class MainActivity : AppCompatActivity(), ItemClickListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var mSchoolViewModel: SchoolViewModel
    private lateinit var schoolDiffUtilListAdapter: SchoolDiffUtilListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        mSchoolViewModel = ViewModelProvider(this).get(SchoolViewModel::class.java)
        mSchoolViewModel.loadSchools()
        Thread.sleep(5000)
        mSchoolViewModel.getSchools()
        binding.recyclerview.layoutManager = LinearLayoutManager(this)
        schoolDiffUtilListAdapter = SchoolDiffUtilListAdapter()
        binding.recyclerview.adapter = schoolDiffUtilListAdapter
        mSchoolViewModel.allSchools.observe(this, Observer {
            val mAllSchools: List<School>? = mSchoolViewModel.allSchools.value
            schoolDiffUtilListAdapter.submitList(mAllSchools)
        })
        schoolDiffUtilListAdapter.setItemClickListener(this)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    override fun onSchoolSelected(school: School) {
        intent = Intent(applicationContext, SchoolActivity::class.java)
        intent.putExtra("SchoolDbn", school.dbn)
        startActivity(intent)
    }


}